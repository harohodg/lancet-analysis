'''
Functions for interating with database used to store information about the Lancet Journal

Author : Harold Hodgins (hhodgins@wlu.ca)
'''
from sqlite3 import Error, IntegrityError
import sqlite3, zlib, re, logging

logger = logging.getLogger(__name__)


SQL_COMMANDS = {
    'GET_URLS' : 'SELECT url from page_urls where url like "%/{}/{}/%"',
    'SAVE_PAGE': 'INSERT INTO raw_pages (url_id, raw_compressed_page) VALUES (?,?)',
    'SAVE_URL' : 'INSERT INTO page_urls (url) VALUES (?)',
    'GET_URLS_WITHOUT_PAGES' : 'SELECT * from urls_without_pages where url like ?',
    'GET_RAW_ISSUE_PAGES': 'SELECT raw_compressed_page from page_urls join raw_pages on page_urls.url_id = raw_pages.url_id where url like "%/{}/issue/%"',
    'GET_UNPARSED_ISSUE_PAGES' : 'SELECT * from unparsed_issue_pages',
    'GET_UNPARSED_ARTICLE_PAGES' : 'SELECT * from unparsed_article_pages',
    'GET_UNPARSED_AFFILIATIONS' :  'SELECT * FROM unparsed_affiliations',
    'GET_UNPARSED_NAMES' : 'SELECT * FROM unparsed_names',
    'GET_AUTHORS_WITHOUT_GENDER' : 'SELECT * FROM authors_without_gender',
    'STORE_ISSUE' : 'INSERT INTO issues(raw_page_id, date, volume, issue, series) VALUES (?,?,?,?,?)',
    'STORE_ARTICLE' : 'INSERT INTO articles (raw_page_id, type_id, title, doi) VALUES (?,?,?,?)',
    'STORE_ARTICLE_TYPE' : 'INSERT INTO article_types (publication_type) VALUES (?)',
    'STORE_AUTHOR': 'INSERT INTO authors (author) VALUES (?)',
    'STORE_AFFILIATION': 'INSERT INTO affiliations (affiliation) VALUES (?)',
    'STORE_AUTHOR_AFFILIATION': 'INSERT INTO author_affiliation (author_id, affiliation_id) VALUES (?,?)',
    'STORE_ARTICLE_AUTHOR': 'INSERT INTO article_authors (article_id, author_affiliation_id) VALUES (?,?)',
    'STORE_AUTHOR_ARTICLE_RANK': 'INSERT INTO Author_Article_Rank (author_id, article_id, rank) VALUES (?,?,?)',
    'STORE_COUNTRY' : 'INSERT into countries (code, alpha_2, alpha_3, name, official_name) VALUES (?,?,?,?,?)',
    'STORE_AFFILIATION_COUNTRY' : "INSERT INTO affiliation_country (affiliation_id, country_id) VALUES (?,?)",
    'STORE_FIRST_NAME' : 'INSERT into first_names (first_name) VALUES (?)',
    'STORE_LAST_NAME' : 'INSERT into last_names (last_name) VALUES (?)',
    'STORE_SPLIT_NAME' : 'INSERT into split_names (raw_name_id, first_name_id, last_name_id) VALUES (?,?,?)',
    'STORE_GENDER' : 'INSERT into genders (gender) VALUES (?)',
    'STORE_AUTHOR_GENDER' : 'INSERT into author_gender (author_id, gender_id, scale) VALUES (?,?,?)',
    'GET_ISSUE_ID' : 'SELECT issue_id FROM full_issues where url = ?',
    'GET_URL_ID'   : 'SELECT url_id FROM page_urls where url = ?',
    'STORE_ISSUE_ARTICLE_URL' : 'INSERT INTO issue_article_urls (issue_id, article_url_id) VALUES (?,?)'
        }


def insert_data(conn, sql_command , data_tuple):
    '''
    Function to insert data into the database. Returns the row id of the inserted data

    Preconditions:
        conn(database connection): The database connection to use
        sql_command(string):The command to execute
        data_tuple(tuple): Tuple of data to insert
    Postconditions:
        returns result(int): The row id of the inserted data. If already in database the row id of the existing data
        returns None if something goes terrible wrong
    '''
    sql_command = sql_command.upper()
    assert sql_command.startswith("INSERT INTO"),"SQL command must start with INSERT INTO"
    result = None
    cursor = conn.cursor()
    try:
        cursor.execute(sql_command, data_tuple)
        result=cursor.lastrowid
    except IntegrityError:
        try:
            m=re.search('INSERT INTO (.*?) (\(.*?\)) VALUES .*',sql_command)
            if m:
                table   = m.group(1)
                columns = tuple( m.group(2)[1:-1].split(",") )
                where_clause = "= ? and ".join(columns) + " = ?"

                cursor.execute("PRAGMA table_info('{}')".format(table))
                results = cursor.fetchone()
                id_column = results[1]
                new_sql_command = 'SELECT {} from {} where {}'.format(id_column, table, where_clause)

                cursor.execute(new_sql_command, data_tuple)
                results = cursor.fetchone()
                result  = results[0]
        except Exception as e:
            logger.error("WHOOPS \n error ={} \n old command {}\n new command = {}\n data = {}".format(e, sql_command, new_sql_command, data_tuple))
    except Exception as e:
        command = new_sql_command if 'new_sql_command' in locals() else sql_comman
        logger.error("{} WHHOOOPS with {} and {}".format(e, command, data_tuple))
    return result

def execute_sql_command(conn, sql_command, data_tuple=()):
    '''
    Function to execute an sql command.

    Preconditions:
        conn(database connection): The database connection to use
        sql_command(string):The command to execute
        data_tuple(tuple): Tuple of data to insert if any
    Postconditions:
        returns result(row_id or list):False is an exception occurs, True if successfull insertion, list if select ... command
        print exception if one occurs
    '''
    logger.debug('Executing {} '.format(sql_command) )
    result = True
    try:
        cursor = conn.cursor()
        cursor.execute(sql_command,data_tuple)
        if sql_command.startswith("SELECT"):
            result = cursor.fetchall()
    except Exception as e:
        logger.error(e)
    return result

def open_database(db_file, enable_foreign=True):
    '''
    Function to return a connection to a sqlite database file

    Preconditions:
        db_file(string) : The database filename
        enable_foreign(boolean) : Do we enable foreign keys before returning the connection. Default is True
    Postconditions:
        returns Connection object or None
    '''
    try:
        conn = sqlite3.connect(db_file)
        if enable_foreign:
            conn.execute("PRAGMA foreign_keys = 1")
        return conn
    except Error as e:
        logger.error(e)
    return None

def close_database():
    '''
    '''
    return


def get_urls(conn, url_type, journal):
    '''
    Function to get a list of all urls of a specific type (issue or article) for journal in the database

    Preconditions:
        conn(database connection) : The database connection to use
        url_type(string): issue, article, ...
        journal(string) : The journal to find issue urls for
    Postconditions:
        returns items(list): List of urls currently in database
    '''
    assert url_type in ('article', 'issue'), '{} is not equal to article or issue'.format(url_type)
    results = execute_sql_command(conn, SQL_COMMANDS['GET_URLS'].format(journal,url_type) )
    return [ item[0] for item in results]


def save_page(conn, url_id, raw_html):
    '''
    Function to insert a compressed copy of the raw html for a page into the database

    Preconditions:
        conn(database connection) : The database connection to use
        url_id(int): The url id of the url associated with the page
        raw_html(string): The raw html to compress and store
    Postconditions:
        returns True if successfull else False
    '''
    compressed_page = zlib.compress(raw_html.encode("utf-8"))
    return execute_sql_command(conn, SQL_COMMANDS['SAVE_PAGE'], (url_id, compressed_page))


def save_url(conn, page_url):
    '''
    Function to add a url to the database

    Preconditions:
        conn(database connection) : The database connection to use
        page_url(string): The url to save
    Postconditions:
        returns True if successfull else False
    '''
    return execute_sql_command(conn, SQL_COMMANDS['SAVE_URL'], (page_url,) )

def save_urls(conn, urls):
    '''
    Function to add a list of urls to the database

    Preconditions:
        conn(database connection) : The database connection to use
        urls(list): The urls to save
    Postconditions:
        returns True if successfull else False
    '''
    result = True
    for url in urls:
        result = result and save_url(conn, url)
    return result

def get_urls_without_pages(conn, page_type=''):
    '''
    Function to return any urls and their ids which don't have associted pages

    Preconditions:
        conn(database connection) : The database connection to use
        page_type(string): What sort of url to look for. (eg issue or article)
    Postconditions:
        returns urls without ...
    '''
    page_type = '%' if page_type == '' else '%{}%'.format(page_type)
    return execute_sql_command(conn, SQL_COMMANDS['GET_URLS_WITHOUT_PAGES'], (page_type,) )



def get_issue_pages(conn, journal):
    '''
    Function to return all of the raw issue pages for a given journal in the database

    Preconditions:
        conn(database connection) : The database connection to use
        journal(string):The journal to search for
    Postconditions:
        returns results(list): A list of the raw compressed issue pages
    '''
    results = execute_sql_command(conn, SQL_COMMANDS['GET_RAW_ISSUE_PAGES'].format(journal) )
    return [item[0] for item in results]


def get_unparsed_issue_pages(conn):
    '''
    Function to return all unparsed issue pages in the database in compressed format.

    Preconditions:
        conn(database connection): The database connection to use
    Postcondtions:
        returns results(list of tuples): (page_id, compressed_page)
    '''
    return execute_sql_command(conn, SQL_COMMANDS['GET_UNPARSED_ISSUE_PAGES'] )

def get_unparsed_article_pages(conn):
    '''
    Function to return all unparsed article pages in the database in compressed format.

    Preconditions:
        conn(database connection): The database connection to use
    Postcondtions:
        returns results(list of tuples): (page_id, compressed_page)
    '''
    return execute_sql_command(conn, SQL_COMMANDS['GET_UNPARSED_ARTICLE_PAGES'] )

def store_issue(conn, issue_data):
    '''
    Function to store the parsed results for a raw issue page

    Precondtions:
        conn(database connection): The database connection to use
        issue_data(tuple): The data to store ( raw_page_id, issue_date, issue_volume, issue_issue, issue_series )
    Postconditions:
        returns the row_id of the inserted issue, None if something went Wrong
    '''
    return insert_data(conn, SQL_COMMANDS['STORE_ISSUE'], issue_data)


def store_authors(conn, authors):
    '''
    Function to store authors parsed from article pages

    Precondtions:
        conn(database connection): The database connection to use
        authors(dict) : keys = author names, values = list of affiliations [] for N
    Postconditions:
        returns list of author_affiliation ids
    '''
    result = []
    for author in authors:
        author_id      = insert_data(conn, SQL_COMMANDS['STORE_AUTHOR'], (author, ) )
        affiliations    = authors[author]
        for affiliation in affiliations:
            affiliation_id =  insert_data(conn, SQL_COMMANDS['STORE_AFFILIATION'], (affiliation, ) )
            result.append( insert_data(conn, SQL_COMMANDS['STORE_AUTHOR_AFFILIATION'], (author_id, affiliation_id) ) )
    return result

def store_article(conn, article_data):
    '''
    Function to store the parsed results for a raw article page

    Precondtions:
        conn(database connection): The database connection to use
        article_data(tuple): The data to store ( raw_page_id, issue_url, article_type, article_title, article_doi, article_authors = [] )
    Postconditions:
        returns the row_id of the inserted article, None if something went Wrong
    '''
    to_store = list(article_data[:-1])
    to_store[1] = insert_data(conn, SQL_COMMANDS['STORE_ARTICLE_TYPE'], (to_store[1], ) )
    author_affiliation_ids = store_authors(conn, article_data[-1])

    article_id = insert_data(conn, SQL_COMMANDS['STORE_ARTICLE'], to_store )


    for rank, author in enumerate(article_data[-1]):
        author_id = insert_data(conn, SQL_COMMANDS['STORE_AUTHOR'], (author, ) )
        insert_data(conn, SQL_COMMANDS['STORE_AUTHOR_ARTICLE_RANK'], (author_id, article_id, rank+1 ) )


    for aa_id in author_affiliation_ids:
        execute_sql_command(conn, SQL_COMMANDS['STORE_ARTICLE_AUTHOR'], (article_id, aa_id))
    return article_id

def get_unparsed_affiliations(conn):
    '''
    Function to return all unparsed affiliations in the database.

    Preconditions:
        conn(database connection): The database connection to use
    Postcondtions:
        returns results(list of tuples): (affiliation_id, affiliation)
    '''
    return execute_sql_command(conn, SQL_COMMANDS['GET_UNPARSED_AFFILIATIONS'] )

def get_unparsed_names(conn):
    '''
    Function to return all unparsed names in the database.

    Preconditions:
        conn(database connection): The database connection to use
    Postcondtions:
        returns results(list of tuples): (name_id, full_name)
    '''
    return execute_sql_command(conn, SQL_COMMANDS['GET_UNPARSED_NAMES'] )



def store_country(conn, country):
    '''
    Function to store a country in the database

    Precondtions:
        conn(database connection): The database connection to use
        country(tuple): The country data to store ( alpha_2, alpha_3, name, code, full_name )
    Postconditions:
        returns the row_id of the inserted country, None if something went Wrong
    '''
    return insert_data(conn, SQL_COMMANDS['STORE_COUNTRY'], country )



def store_affiliation_country(conn, data):
    '''
    Function to store an affiliation / country association in the database

    Precondtions:
        conn(database connection): The database connection to use
        data(tuple): The data to store ( affiliation_id, country_id )
    Postconditions:
        returns the row_id of the inserted data, None if something went Wrong
    '''
    return insert_data(conn, SQL_COMMANDS['STORE_AFFILIATION_COUNTRY'], data )


def store_names(conn, name_id, first_name, last_name):
    '''
    Function to store an author -> firstname, lastname association (and the first/last names) in the database

    Precondtions:
        conn(database connection): The database connection to use
        name_id(int): The raw name id to associate with the following first/last names
        first_name(string): The first name to store and associate
        last_name(string): The last name to store and associate
    Postconditions:
        returns nothing
    '''
    first_name_id = insert_data(conn, SQL_COMMANDS['STORE_FIRST_NAME'], (first_name,) )
    last_name_id  = insert_data(conn, SQL_COMMANDS['STORE_LAST_NAME'], (last_name,) )
    return execute_sql_command(conn, SQL_COMMANDS['STORE_SPLIT_NAME'], (name_id, first_name_id, last_name_id) )


def get_authors_without_gender(conn):
    '''
    Function to return all authors who don't have a gender in the database

    Precondtions:
        conn(database connection): The database connection to use
    Postconditions:
        returns list of authors and their ids without an associated gender [(author_id, author_full_name), ...]
    '''
    return execute_sql_command(conn, SQL_COMMANDS['GET_AUTHORS_WITHOUT_GENDER'] )

def store_gender(conn, gender):
    '''
    Function to store an gender in the database

    Precondtions:
        conn(database connection): The database connection to use
        gender(string): The gender to store
    Postconditions:
        returns the id of the inserted gender
    '''
    return insert_data(conn, SQL_COMMANDS['STORE_GENDER'], (gender,) )

def store_author_gender(conn, author_id, gender, scale):
    '''
    Function to store an author and associated namsor gender and gender scale in the database

    Precondtions:
        conn(database connection): The database connection to use
        author_id(int): The author_id to store
        gender(string): The gender to store
        scale(float)  : The gender scale to store
    Postconditions:
        returns nothing
    '''
    gender_id = store_gender(conn, gender)
    return execute_sql_command(conn, SQL_COMMANDS['STORE_AUTHOR_GENDER'], (author_id, gender_id, scale) )



def get_url_id(conn, url):
    '''
    Function to return the url id for a given url

    Precondtions:
        conn(database connection): The database connection to use
        url(string) : The url to search for
    Postconditions:
        returns result (int if found else None)
    '''
    logger.debug("Searching for id for {}".format(url))
    url_id = execute_sql_command(conn, SQL_COMMANDS['GET_URL_ID'], (url,) )
    logger.debug("GOT {}".format(url_id))
    return int(url_id[0][0]) if url_id != [] else None

def store_issue_article_urls(conn, issue_id, article_urls):
    '''
    Function to store an issue_id and associated article_url_ids

    Precondtions:
        conn(database connection): The database connection to use
        issue_id(int): The issue_id to store
        article_urls(list) : list of raw_urls to store link
    Postconditions:
        returns True if all went well else False
    '''
    result = True
    for url in article_urls:
        url_id = get_url_id(conn, url)
        if url_id is not None:
            result = result and execute_sql_command(conn, SQL_COMMANDS['STORE_ISSUE_ARTICLE_URL'], (issue_id, url_id) )
        else:
            result = False
    return result