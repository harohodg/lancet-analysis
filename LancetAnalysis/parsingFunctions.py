'''
Functions for parsing the raw pages and raw results (ie affiliations and names) and storing the results in the database
'''
import logging, traceback
from . import databaseFunctions
from . import genderFunctions
from bs4 import BeautifulSoup
import zlib, re, time
import pycountry
from tqdm import tqdm

LANCET_ROOT            = 'https://www.thelancet.com'
COUNTRIES_TO_TRANSLATE = {"UK" : "United Kingdom"}
logger = logging.getLogger(__name__)

def parseIssues(conn):
    '''
    Function for parsing all of the raw issue pages and storing the results in the database

    Preconditions:
        conn(database connection): The database connection to use.
    Postconditions:
        Returns True if all successfull else False
    '''
    result = True
    logger.info("Parsing isssue pages")
    for page_id, compressed_page in tqdm( databaseFunctions.get_unparsed_issue_pages(conn), desc='Parsing Issues'):
        logger.debug("Parsing raw page {} as an issue page".format(page_id))
        decompressed_page = zlib.decompress(compressed_page).decode('utf-8')
        soup = BeautifulSoup( decompressed_page, "lxml")
        try:
            issue_date   = soup.find('h1',{'class':'toc-header__issue-date'})
            issue_volume = issue_date.findNext('span')
            issue_issue  = issue_volume.findNext('span')
            issue_series = issue_issue.findNext('span')
            issue_data   = ( page_id, issue_date.text, issue_volume.text, issue_issue.text, issue_series.text )
            issue_id = databaseFunctions.store_issue(conn, issue_data  )
            logger.debug("Issue stored with id = {}".format(issue_id))

            logger.debug('Extracting article urls')
            article_urls = list( set(LANCET_ROOT+url['href'] for url in soup.find_all('a',href=re.compile( '/article/' ) ) ) )
            logger.debug('Extracted {} article_urls'.format(len(article_urls) ))

            logger.debug('Adding new article urls to database')
            current_article_urls = databaseFunctions.get_urls(conn,'article','%')
            logger.debug('There are {} article urls currently in the database'.format(len(current_article_urls)) )

            new_article_urls     = [ url for url in article_urls if url not in current_article_urls]
            logger.debug('There are {} new article urls to store'.format(len(new_article_urls)) )


            urls_saved = databaseFunctions.save_urls(conn, new_article_urls)
            logger.debug('Successfully saved new article urls to database = {}'.format(urls_saved) )

            logger.debug('Storing links between article urls and issue id')
            urls_stored = databaseFunctions.store_issue_article_urls(conn, issue_id, article_urls )
            result = result and urls_stored
            logger.debug('Successfully stored = {}'.format(urls_stored) )
        except Exception as e:
            logger.error("Unable to parse issue page {} because {}".format(page_id, e))
            exceptiondata = traceback.format_exc().splitlines()
            logger.debug(exceptiondata[-3:])
            result = False
        conn.commit()
    return result


def parseArticles(conn):
    '''
    Function for parsing all of the raw article pages and storing the results in the database

    Preconditions:
        conn(database connection): The database connection to use.
    Postconditions:
        Returns True if all successfull else False
    '''
    result = True

    logger.info("Parsing article pages")
    for page_id, compressed_page in tqdm( databaseFunctions.get_unparsed_article_pages(conn), desc='Parsing Articles' ):
        logger.debug("Parsing raw page {} as an article page".format(page_id))
        decompressed_page = zlib.decompress(compressed_page).decode('utf-8')
        soup = BeautifulSoup( decompressed_page, "lxml")
        try:
            authors = {}
            for item in soup.find_all("meta", {'name':'citation_author'}):
                authors[item['content']] = []

            for item in soup.find_all("meta", {'name':'citation_author_institution'}):
                affiliation= item["content"]
                author = item.findPreviousSibling("meta", {'name':'citation_author'})['content']
                authors[author].append(affiliation)

            article_type      = soup.find('span',{'class':'article-header__journal'})
            article_issue_url = soup.find('a', {'class':'article-header__vol faded'})['href']
            article_title     = soup.find('h1', {'class':'article-header__title'})
            article_doi       = soup.find('a',{'class':'article-header__doi__value'})['href']

            article_data = (page_id, article_type.text, article_title.text, article_doi, authors)
            result = result and databaseFunctions.store_article(conn, article_data  )
        except Exception as e:
            logger.error("Unable to parse article page {} because {}".format(page_id, e))
            result = False
        conn.commit()
    return result


def parseAffiliations(conn):
    '''
    Function for parsing all of the raw affiliations and determining their associated country
    General idea is to take the last chunk of the affiliation after the last , and do a pycountry.countries.lookup() on it.
    If not found we set the country to be "Unknown" for now

    If country official name doesn't exist it's set to the same as the country name


    Preconditions:
        conn(database connection): The database connection to use.
    Postconditions:
        Returns True if all successfull else False
    '''
    result = True

    unparsed_affiliations = databaseFunctions.get_unparsed_affiliations(conn)
    logger.info("Parsing {} Affiliations ".format(len(unparsed_affiliations)))
    for affiliation_id, affiliation in tqdm( unparsed_affiliations, desc='Parsing Affiliations' ):
        logger.debug("{} -> {}".format(affiliation_id, affiliation))
        country = (-1, "N","N","Unknown","Unknown")
        try:
            lookup_value = affiliation.split(",")[-1].strip()
            lookup_value = COUNTRIES_TO_TRANSLATE.get(lookup_value, lookup_value)

            c = pycountry.countries.lookup(lookup_value)
            official_name = c.official_name if hasattr(c, "official_name") else c.name
            country = (c.numeric, c.alpha_2, c.alpha_3, c.name, official_name)
        except LookupError:
            pass
        country_id = databaseFunctions.store_country(conn, country )
        conn.commit()
        logger.debug("Affiliation_id = {}, country_id = {}".format(affiliation_id, country_id) )
        result = result and databaseFunctions.store_affiliation_country(conn, (affiliation_id, country_id) )
        conn.commit()
    return result


def parseNames(conn):
    '''
    Function for parsing all of the raw names and splitting them into first and lastnames
    First split by spaces. Assume the last chunk is the last name and the first name is the first chunk from the left which isn't all CAPS, doesn't have a period or comma in it, and is at least two characters long

    Preconditions:
        conn(database connection): The database connection to use.
    Postconditions:
        Returns True if all successfull else False
    '''
    result = True

    logger.info("Splitting names")
    for name_id, full_name in tqdm( databaseFunctions.get_unparsed_names(conn), desc='Splitting Names' ):
        split_name = full_name.split(" ")
        last_name  = split_name[-1]
        first_name = "Unknown"
        if len(split_name) >= 2:
            index = 0
            while index < len(split_name) -1 and first_name == "Unknown":
                chunk = split_name[index]
                if not chunk.isupper() and "." not in chunk and "," not in chunk and len(chunk) >= 2:
                    first_name = chunk
                index += 1
        logger.debug("{} -> {} -> |{}|{}|".format(name_id, full_name, first_name, last_name))
        result = result and databaseFunctions.store_names(conn, name_id, first_name, last_name)
        conn.commit()
    return result

def parseGenders(conn):
    '''
    Function for obtaining and store Namsor gender for each author in the database

    Preconditions:
        conn(database connection): The database connection to use.
    Postconditions:
        Returns True if all successfull else False
    '''
    result = True

    logger.info("Retrieving Namsor Genders")
    authors_sans_gender = databaseFunctions.get_authors_without_gender(conn)
    while authors_sans_gender != []:
        authors_sans_gender = authors_sans_gender[:1000]
        namsor_results = genderFunctions.get_namsor_genders(full_names = [item[1] for item in authors_sans_gender] )
        for index, namsor_data in enumerate( tqdm( namsor_results, desc='Storing Genders' ) ):
            logger.debug("Storeing Namsor Gender for {}".format(authors_sans_gender[index][1]))
            author_id = authors_sans_gender[index][0]
            result = result and databaseFunctions.store_author_gender(conn, author_id, namsor_data.get("likelyGender"), namsor_data.get("genderScale") )
            conn.commit()
        authors_sans_gender = databaseFunctions.get_authors_without_gender(conn)
    return result
