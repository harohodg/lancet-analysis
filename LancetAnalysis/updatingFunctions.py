'''
Functions used to update raw data in database.

'''
import logging
from . import databaseFunctions
from . import websiteFunctions

logger = logging.getLogger(__name__)


def update_raw_pages(conn, journal, page_type):
    '''
    Function to update the raw issue and articles in the database for a given journal
    Preconditions:
        conn(database connection): The database connection to use
        journal(string): The journal to update the issue urls for
        page_type(string): The page type to update (issue, or article)
    Postconditions:
        returns nothing
    '''
    assert page_type in ("issue", "article"), "page_type must be one of 'issue' or 'article'"
    get_urls = websiteFunctions.get_issue_urls if page_type == 'issue' else websiteFunctions.get_article_urls

    current_urls = databaseFunctions.get_urls(conn, page_type, journal)
    logger.debug("Found {} {} urls for {} in the database".format( len(current_urls), page_type, journal) )

    urls = get_urls(journal, conn)
    new_urls = [url for url in urls if url not in current_urls]
    logger.debug("Found {} new {} urls for {}.".format( len(new_urls), page_type, journal) )

    for url in new_urls[:100]:
        logger.debug('Adding {} to the database'.format(url))
        databaseFunctions.save_url(conn,url)
    conn.commit()

    logging.info("Downloading raw pages for any {} urls not yet in database".format(page_type))
    websiteFunctions.update_pages(conn)
    return
