'''
Functions for interating with the Lancet Journal website

Author : Harold Hodgins (hhodgins@wlu.ca)
'''
import traceback, logging
import requests, re, datetime, zlib, time
from tqdm import tqdm
from requests_futures.sessions import FuturesSession
from bs4 import BeautifulSoup
from . import databaseFunctions

LANCET_ROOT        = 'https://www.thelancet.com'
LANCET_ISSUES_ROOT = 'https://www.thelancet.com/journals/{}/issues'
START_DECADE       = 202
MAX_CONNECTIONS    = 10

logger = logging.getLogger(__name__)

def get_issue_urls(conn, journal):
    '''
    Function to download list of urls to all issues on Lancet from a specific sub journal and store them in the database
    Preconditions:
        conn(database connection): The database connection to use
        journal(string): The journal to find the issue links for
    Postconditions:
        Returns True if all successful else False
    '''
    logger.info("Getting issue urls for {} journal on lancet".format(journal))

    now  = datetime.datetime.now()
    end_decade = int(str(now.year)[:-1])+1

    url = LANCET_ISSUES_ROOT.format(journal) + "#" + "&".join( ["loi_decade_{}".format(d) for d in range(START_DECADE, end_decade )] )
    #Generate a url of the form https://www.thelancet.com/journals/lancet/issues#decade=loi_decade_182&decade=loi_decade_183&....
    try:
        r = requests.get(url)
        soup = BeautifulSoup(r.text, "lxml")
        href_pattern = '{}/issue'.format(journal)
        issue_urls = list( set(LANCET_ROOT+url['href'] for url in soup.find_all('a',href=re.compile(href_pattern) ) ) )
        logger.debug("Found {} issue urls".format( len(issue_urls) ) )
    except Exception as e:
        logger.error(e)
        return False

    #Now try and store any new urls
    try:
        current_urls = databaseFunctions.get_urls(conn, 'issue', journal)
        new_urls = [url for url in issue_urls if url not in current_urls]
        return databaseFunctions.save_urls(conn, new_urls)
    except Exception as e:
        logger.error(e)
        return False


def get_pages(conn, num_pages=None, page_type=''):
    '''
    Function to download and store the raw html for any urls in the database that don't have an associated page.

    Preconditions:
        conn(database connection): The database connection to use
        num_pages(int): How many pages to download. (default = None for all)
    Postconditions:
        return result(boolean) True if all successfull else False
    '''

    session = FuturesSession(max_workers=MAX_CONNECTIONS)
    result = True

    urls_without_pages = databaseFunctions.get_urls_without_pages(conn, page_type)
    urls_to_download   = urls_without_pages if num_pages is None else urls_without_pages[:num_pages]

    responses = (session.get(u[1]) for u in urls_to_download)

    logger.info('Downloading {} pages for {} urls in the database without pages.'.format(page_type, len(urls_to_download)) )
    for index, response in enumerate(tqdm(responses, total=len(urls_to_download), desc='Downloading pages.') ):
        try:
            page = response.result()
            assert page.url == urls_without_pages[index][1]
            if page.status_code is 200:
                url_id = urls_without_pages[index][0]
                text   = page.text
                result = result and databaseFunctions.save_page(conn, url_id, text)
                conn.commit()
            else:
                result = False
        except:
            logger.error('Trying to download {} got {}'.format(urls_without_pages[index][1], page.url))
            result = False
            continue

    logger.debug('All pages downloaded and stored properly = {}'.format(result) )
    conn.commit()
    return result