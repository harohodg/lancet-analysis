'''
Functions to download gender information from

Namsor https://www.namsor.com/
Seems to allow general requests to https://api.namsor.com/onomastics/api/json/gender/{firstname}/{lastname}
Officially supposed to use API with API key to get 5000 free per month + 0.5cents each beyond

and

Genderize https://genderize.io/
GET https://api.genderize.io/?name=kim
GET https://api.genderize.io/?name=kim&country_id=dk

Webpage claims it returns country code when given. From actual testing doesnot seem to.
Although invalid codes do seem to confuse it and result in gender=None

'''
import requests
from .API_credentials import namsor_API_KEY


#NAMSOR_BASE_URL_WITHOUT_API = 'https://api.namsor.com/onomastics/'
NAMSOR_BASE_URL_WITH_API    = 'https://v2.namsor.com/NamSorAPIv2/'
NAMSOR_URLS = {'SPLIT_NAME_WITHOUT_COUNTRY' : 'api2/json/gender/{firstName}/{lastName}',
               'SPLIT_NAME_WITH_COUNTRY'    : 'api2/json/genderGeo/{firstName}/{lastName}/{countryIso2}',
               'FULL_NAME_WITHOUT_COUNTRY'  : 'api2/json/genderFull/{fullName}',
               'FULL_NAME_WITH_COUNTRY'     : 'api2/json/genderFullGeo/{fullName}/{countryIso2}'
              }


def get_genderize_gender(fname, alpha_2 = None, api_key = None):
    '''
    Function to query the genderize API using a first name and optional alpha_2 country code
    Makes no attempt to validate country code if given

    Preconditions:
        fname(string) : The first name to query
        alpha_2(string) : The alpha_2 version of the country code (eg US). Default None
        api_key(string) : The API key to use. Default None (currently not used)
    Postconditions:
        returns {"name":"??","gender":"??","probability":"??","count":??}, calls remaining, time till next batch in seconds
    '''
    url  = "https://api.genderize.io/?name={}".format(fname)
    url += "&country_id={}".format(alpha_2) if alpha_2 is not None else ""

    r = requests.get(url)
    if r.status_code != 200:
        raise Exception('Genderize API request to {} returned status code {}'.format(r.url, r.status_code))
    else:
        return r.json(), int(r.headers['X-Rate-Limit-Remaining']), int(r.headers['X-Rate-Reset'])

def get_namsor_gender(full_name = None, fname = None, lname = None, alpha_2 = None, api_key = namsor_API_KEY):
    '''
    Function to query the namsor API using a full name or a first name and optional alpha_2 country code
    Makes no attempt to validate country code if given

    Preconditions:
        full_name(string) : The full name to query
        fname(string) : The first name to query
        lname(string) : The last  name to query
        alpha_2(string) : The alpha_2 version of the country code (eg US). Default None
        api_key(string) : The API key to use. Default (whatever is set in API_credentials file)
    Postconditions:
        returns {'scale':-1 to 1,  'gender': '??',  'firstName': '??', 'lastName': '??', 'id': '??'} can ignore id
    '''
    assert api_key is not None, "API KEY MUST be provided"
    assert ( full_name is not None and fname is None and lname is None ) or ( full_name is None and fname is not None and lname is not None), "Must provide full name or first and last name. Not Both"

    base_url =  NAMSOR_BASE_URL_WITH_API if api_key is not None else NAMSOR_BASE_URL_WITHOUT_API#This will fail if no API key given
    url_key  = 'FULL_NAME' if full_name is not None else 'SPLIT_NAME'
    url_key += '_WITH_COUNTRY' if alpha_2 is not None else '_WITHOUT_COUNTRY'

    base_url += NAMSOR_URLS[url_key]

    url_values = {'firstName':fname, 'lastName':lname, 'fullName':full_name, 'countryIso2':alpha_2 }
    url     = base_url.format( **url_values )
    headers = {'accept': 'application/json' , 'X-API-KEY' : api_key} if api_key is not None else {}

    r = requests.get(url, headers=headers)
    if r.status_code != 200:
        raise Exception('Namsor API request to {} with headers {} returned status code {}'.format(r.url, r.headers, r.status_code))
    else:
        return r.json()

def get_namsor_genders(full_names, api_key = namsor_API_KEY):
    '''
    Function to take a list of fullnames and return a corresponding list of gender information.

    Preconditions:
        full_names(list) : The full names to query
        api_key(string)  : The API key to use. Default (whatever is set in API_credentials file)
    Postconditions:
        returns list of {"name": "string", "likelyGender": "male", "genderScale": 0, "score": 0, "id":"string"} in the same order as sent in
    '''
    base_url =  NAMSOR_BASE_URL_WITH_API if api_key is not None else NAMSOR_BASE_URL_WITHOUT_API#This will fail if no API key given
    url      = base_url + 'api2/json/genderFullBatch/'

    headers = {'accept': 'application/json' , 'X-API-KEY' : api_key} if api_key is not None else {}

    data_to_send = { "personalNames": [ {"id": index, "name": name } for index, name in enumerate(full_names) ] }

    r = requests.post(url, headers=headers, json=data_to_send)
    if r.status_code != 200:
        raise Exception('Namsor API request to {} with headers {} returned status code {}'.format(r.url, r.headers, r.status_code))
    else:
        returned_data = r.json()
        results = returned_data['personalNames']

        return results

