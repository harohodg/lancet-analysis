Code to analyse articles in the Lancet Journal.

## Getting Started
- Clone this repository
- Run pip3 install -r requirements.txt to install the required libraries (use python virtual enviroments or run in a docker container if you want to keep stuff tidy)
- Rename LancetAnalysis\API_credentials.py.example to LancetAnalysis.py\API_credentials.py and edit as necessary
- Run 'python3 init_database.py' to setup the empty database
- Run 'python3 update_data.py' to fill the database. This will take awhile.
- Run export_data.sh to dump the full data set into full_data.csv
- Rerun the last two lines periodically if you want to update the dataset (full_data.csv will be wiped out each time)

## Overall Algorithm
- Obtain a list of all new issue urls
- Download any pages for issue urls not already in the database
- Parse every new issue page and store the issue date, volume, issue, series, and article urls (linked to issue id) in the database
- Download any pages for article urls not already in the database
- Parse every new article page and store authors (with article rank) and affiliations, article_type, title, and doi
- Parse every new affiliations in the database and store associated country
- Parse every new author and split into first & last name
- For every author without a gender using [Namsor](https://www.namsor.com/)


## Key Methods / Assumptions
- Foreign key support is turned on in the database when it's connected to
- raw pages are stored compressed using zlib.compress(raw_html.encode("utf-8")) and decompressed using zlib.decompress(compressed_page).decode('utf-8')
- Issue date,  volume, issue, series, and article links are extracted using BeautifulSoup4
- Article authors and affiliations, type, title, and doi are extracted using BeautifulSoup4
- Affiliation Country is assumed to be the chunk after the last space. UK is replaced with United Kingdom before searching pycountry for the relavent country information
- When splitting names we assume the last name is the chunk after the last space and the first name is the first chunk length > 2 which is not all uppercase and doesn't contain . or , otherwise set as Unknown
- When using the Namsor API use the author full name as retrieved from the article and no country information

## Database Layout
![](Lancet_Authors_Layout.png)

## TODO
- Add command line arguments to change the number of pages downloaded at once and the target journal
- Parse pages in parrallel
- Find a way to make dealing with website changes easier to handle
- Make database nameing scheme consistent

