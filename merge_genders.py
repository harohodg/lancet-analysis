from LancetAnalysis import databaseFunctions

OLD_DATABASE = '../lancet-analysis/lancet_authors.db'

with databaseFunctions.open_database('./lancet_authors.db') as database:
    database.execute("ATTACH DATABASE ? AS old_db",(OLD_DATABASE,))


    #Move old gender table to new gender table
    database.execute('INSERT INTO genders SELECT * FROM old_db.genders;')

    sql_command = '''select author, old_db.genders.gender_id, scale from old_db.authors JOIN
    old_db.author_gender ON old_db.authors.author_id = old_db.author_gender.author_id
    JOIN old_db.genders ON old_db.genders.gender_id = old_db.author_gender.gender_id'''
    old_mapping = database.execute(sql_command).fetchall()


    for author, gender_id, gender_scale in old_mapping:
        try:
            author_id = database.execute('SELECT author_id from authors where author = ?',(author,)).fetchone()[0]
            database.execute('INSERT INTO author_gender (author_id, gender_id, scale ) VALUES (?,?,?)', (author_id, gender_id, gender_scale))
        except Exception as e:
            print('{} not in new database'.format(author))
            continue



    database.commit()