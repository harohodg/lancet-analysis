'''
Script to setup the initial database
'''
from LancetAnalysis.databaseFunctions import open_database

DATABASE_FILE = 'lancet_authors.db'
DATABASE_SCHEMA = '''
CREATE TABLE `page_urls` ( `url_id` INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, `url` TEXT UNIQUE );

CREATE TABLE "raw_pages" ( `raw_page_id` INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, `url_id` INTEGER UNIQUE, `raw_compressed_page` INTEGER, FOREIGN KEY(`url_id`) REFERENCES `page_urls`(`url_id`) ON UPDATE CASCADE ON DELETE CASCADE );

CREATE VIEW urls_without_pages as select page_urls.url_id, url from page_urls left join raw_pages on page_urls.url_id = raw_pages.url_id where raw_pages.url_id is null;

CREATE VIEW article_urls as
select * from page_urls where url like "%/article/%";

CREATE VIEW issue_urls as
select * from page_urls where url like "%/issue/%";

CREATE VIEW raw_article_pages as
select raw_page_id, raw_compressed_page from article_urls join raw_pages on article_urls.url_id = raw_pages.url_id;

CREATE VIEW raw_issue_pages as
select raw_page_id, raw_compressed_page from issue_urls join raw_pages on issue_urls.url_id = raw_pages.url_id;

CREATE TABLE `journals` (
        `journal_id`    INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
        `journal_name`  TEXT UNIQUE
);


CREATE TABLE "article_types" (
        `type_id`       INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
        `publication_type`      TEXT UNIQUE
);


CREATE VIEW unparsed_issue_pages as
select raw_issue_pages.raw_page_id, raw_compressed_page from raw_issue_pages left join issues on raw_issue_pages.raw_page_id = issues.raw_page_id
where issues.issue_id is NULL;

CREATE TABLE "issues" (
        `issue_id`      INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
        `raw_page_id`   INTEGER,
        `date`  TEXT,
        `volume`        TEXT,
        `issue` TEXT,
        `series`        TEXT,
        FOREIGN KEY(`raw_page_id`) REFERENCES `raw_pages`(`raw_page_id`)
);

CREATE VIEW unparsed_article_pages as
select raw_article_pages.raw_page_id, raw_compressed_page from raw_article_pages left join articles on raw_article_pages.raw_page_id = articles.raw_page_id where publication_id is NULL;

CREATE TABLE `authors` (
        `author_id`     INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
        `author`        TEXT NOT NULL UNIQUE
);


CREATE TABLE `affiliations` (
        `affiliation_id`        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
        `affiliation`   TEXT NOT NULL UNIQUE
);


CREATE TABLE "author_affiliation" (
        `author_affiliation_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
        `author_id`     INTEGER NOT NULL,
        `affiliation_id`        INTEGER NOT NULL,
        FOREIGN KEY(`author_id`) REFERENCES `authors`(`author_id`),
        FOREIGN KEY(`affiliation_id`) REFERENCES `affiliations`(`affiliation_id`),
        UNIQUE(`author_id`,`affiliation_id`)
);


CREATE TABLE `article_authors` (
        `article_id`    INTEGER NOT NULL,
        `author_affiliation_id` INTEGER NOT NULL,
        FOREIGN KEY(`article_id`) REFERENCES `articles`(`publication_id`),
        FOREIGN KEY(`author_affiliation_id`) REFERENCES `author_affiliation`(`author_affiliation_id`)
        UNIQUE(`article_id`, `author_affiliation_id`)
);

CREATE TABLE "articles" (
        `publication_id`        INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
        `raw_page_id`   INTEGER UNIQUE,
        `type_id`       INTEGER,
        `title` TEXT,
        `doi`   TEXT UNIQUE,
        FOREIGN KEY(`raw_page_id`) REFERENCES `raw_pages`(`raw_page_id`),
        FOREIGN KEY(`type_id`) REFERENCES `article_types`(`type_id`)
);



CREATE TABLE `Author_Article_Rank` (
        `id`    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
        `author_id`     INTEGER NOT NULL,
        `article_id`    INTEGER NOT NULL,
        `rank`  INTEGER NOT NULL,
        UNIQUE(`author_id`,`article_id`,`rank`),
        FOREIGN KEY(`article_id`) REFERENCES `articles`(`publication_id`),
        FOREIGN KEY(`author_id`) REFERENCES `authors`(`author_id`)
);


CREATE TABLE "countries" (
        `country_id`    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
        `code`  INTEGER NOT NULL UNIQUE,
        `alpha_2`       TEXT NOT NULL UNIQUE,
        `alpha_3`       TEXT NOT NULL UNIQUE,
        `name`  TEXT NOT NULL UNIQUE,
        `official_name` TEXT NOT NULL UNIQUE,
        UNIQUE(`alpha_2`,`alpha_3`,`name`,`code`,`official_name`)
);


CREATE VIEW unparsed_affiliations AS SELECT affiliations.affiliation_id, affiliations.affiliation FROM affiliations LEFT JOIN affiliation_country ON affiliations.affiliation_id = affiliation_country.affiliation_id where affiliation_country.affiliation_id is NULL;

CREATE TABLE "affiliation_country" (
        `affiliation_country_id`        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
        `affiliation_id`        INTEGER NOT NULL UNIQUE,
        `country_id`    INTEGER NOT NULL,
        UNIQUE(`affiliation_id`,`country_id`),
        FOREIGN KEY(`country_id`) REFERENCES `countries`(`country_id`),
        FOREIGN KEY(`affiliation_id`) REFERENCES `affiliations`(`affiliation_id`)
);



CREATE VIEW unparsed_names
AS
SELECT author_id, author FROM
authors LEFT JOIN split_names
ON authors.author_id = split_names.raw_name_id
WHERE raw_name_id is NULL;

CREATE TABLE "first_names" ( `first_name_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `first_name` TEXT NOT NULL UNIQUE );


CREATE TABLE `last_names` ( `last_name_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `last_name` TEXT NOT NULL UNIQUE );


CREATE TABLE "split_names" (
        `raw_name_id`   INTEGER NOT NULL UNIQUE,
        `first_name_id` INTEGER NOT NULL,
        `last_name_id`  INTEGER NOT NULL,
        FOREIGN KEY(`first_name_id`) REFERENCES `first_names`(`first_name_id`),
        PRIMARY KEY(`raw_name_id`,`first_name_id`,`last_name_id`),
        FOREIGN KEY(`raw_name_id`) REFERENCES `authors`(`author_id`),
        FOREIGN KEY(`last_name_id`) REFERENCES `last_names`(`last_name_id`)
);


CREATE TABLE "genders" ( `gender_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `gender` TEXT UNIQUE );


CREATE TABLE `author_gender` ( `author_id` INTEGER NOT NULL UNIQUE, `gender_id` INTEGER NOT NULL, `scale` REAL, FOREIGN KEY(`author_id`) REFERENCES `authors`(`author_id`), FOREIGN KEY(`gender_id`) REFERENCES `genders`(`gender_id`) );

CREATE VIEW authors_without_gender AS
SELECT authors.author_id, authors.author FROM authors LEFT JOIN author_gender
ON authors.author_id = author_gender.author_id
WHERE author_gender.author_id IS NULL;

CREATE VIEW full_article_issue_type as
SELECT article_id, full_articles.url as article_url, title, doi, publication_type, full_issues.url as issue_url, date, volume, issue, series FROM
full_articles JOIN issue_article_urls ON
full_articles.url_id = issue_article_urls.article_url_id JOIN
full_issues ON full_issues.issue_id = issue_article_urls.issue_id;

CREATE VIEW full_author_gender_affilation AS
select article_id, authors.author_id, author, scale, gender, affiliation, code, alpha_2, alpha_3, name, official_name
from article_authors JOIN
author_affiliation ON article_authors.author_affiliation_id = author_affiliation.author_affiliation_id JOIN
authors on authors.author_id = author_affiliation.author_id
JOIN affiliations ON affiliations.affiliation_id = author_affiliation.affiliation_id
JOIN affiliation_country on affiliations.affiliation_id = affiliation_country.affiliation_id
JOIN countries on countries.country_id = affiliation_country.country_id
JOIN author_gender ON author_gender.author_id = authors.author_id
JOIN genders ON genders.gender_id = author_gender.gender_id;

CREATE VIEW full_data as
SELECT Author_Article_Rank.article_id as article_id, title, doi, date, volume, issue, series, publication_type,
full_author_gender_affilation.author_id as author_id, author, rank as author_rank, scale, gender,
affiliation, code, alpha_2, alpha_3, name, official_name
from Author_Article_Rank
JOIN full_article_issue_type on Author_Article_Rank.article_id = full_article_issue_type.article_id
JOIN full_author_gender_affilation ON Author_Article_Rank.author_id = full_author_gender_affilation.author_id and Author_Article_Rank.article_id = full_author_gender_affilation.article_id

order by date, article_id, rank;


CREATE VIEW full_issues AS
SELECT page_urls.url_id, issue_id, url, date, volume, issue, series FROM
page_urls JOIN raw_pages ON
page_urls.url_id = raw_pages.url_id
JOIN issues ON
issues.raw_page_id = raw_pages.raw_page_id;


CREATE TABLE "issue_article_urls" (
    "issue_id" INTEGER NOT NULL,
    "article_url_id" INTEGER NOT NULL UNIQUE,
    FOREIGN KEY("article_url_id") REFERENCES "page_urls"("url_id"),
    FOREIGN KEY("issue_id") REFERENCES "issues"("issue_id")
);

CREATE TRIGGER validate_url_id BEFORE INSERT ON issue_article_urls
BEGIN
 SELECT
 CASE
 WHEN (SELECT count(*)
      FROM article_urls
      WHERE url_id = NEW.article_url_id
     ) = 0 THEN
 RAISE (
 ABORT,
 'Provided url id is not for a article'
 )
 END;
END;


CREATE VIEW full_articles AS
SELECT
page_urls.url_id as url_id, publication_id as article_id, page_urls.url as url, title, doi, publication_type
FROM
articles JOIN article_types ON articles.type_id = article_types.type_id
JOIN raw_pages ON articles.raw_page_id = raw_pages.raw_page_id
JOIN page_urls ON page_urls.url_id = raw_pages.url_id;
'''

conn = open_database(DATABASE_FILE)
with conn:
    conn.executescript(DATABASE_SCHEMA)
