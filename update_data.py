#!/usr/bin/env python3

'''
Program to update the database containing Lancet Articles

Author : Harold Hodgins (hhodgins@wlu.ca)
'''
import zlib, logging
from LancetAnalysis import databaseFunctions
from LancetAnalysis import parsingFunctions
from LancetAnalysis.databaseFunctions import open_database
from LancetAnalysis import websiteFunctions

DATABASE_FILE = 'lancet_authors.db'
JOURNAL       = 'langlo'

# set up logging to file - see previous section for more details
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-50s %(levelname)-10s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='./update_data.log',
                    filemode='w')
# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(logging.INFO)
# set a format which is simpler for console use
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
# tell the handler to use this format
console.setFormatter(formatter)

logger = logging.getLogger(__name__)
logger.addHandler(console)
logging.getLogger('LancetAnalysis.parsingFunctions').addHandler(console)
logging.getLogger('LancetAnalysis.updatingFunctions').addHandler(console)
logging.getLogger('LancetAnalysis.websiteFunctions').addHandler(console)
logging.getLogger('LancetAnalysis.databaseFunctions').addHandler(console)

logging.getLogger('urllib3.connectionpool').setLevel(logging.WARNING)

#get connection to database

#get list of current issue urls
#get list of all issues
#For every issue url found this time add it to the database
#for every url without a page, download and store it
#For every page associated with an issue url extract all article urls
#Store any new ones.
#For every url without a page, download and store it.
#Print results. Found .... new ... ..

#get list of issues
#for every issue get list of articles

#for every article (url) not currently in database
#store in database

#print stats (ie how many new pages downloaded)

#Finally close database


with open_database(DATABASE_FILE) as conn:
    all_successful = True

    all_successful = all_successful and websiteFunctions.get_issue_urls(conn, JOURNAL)

    logger.info("Downloading issue pages")
    all_successful = all_successful and websiteFunctions.get_pages(conn, page_type='issue')

    all_successful = all_successful and parsingFunctions.parseIssues(conn)

    logger.info('Downloading article pages')
    all_successful = all_successful and websiteFunctions.get_pages(conn, page_type='article')

    all_successful = all_successful and parsingFunctions.parseArticles(conn)
    all_successful = all_successful and parsingFunctions.parseAffiliations(conn)
    all_successful = all_successful and parsingFunctions.parseNames(conn)
    all_successful = all_successful and parsingFunctions.parseGenders(conn)

logger.info("Finished updating the raw data in the database")
if not all_successful:
    print('Database was not fully updated. Try running this program again. If this message returns check the log file.')